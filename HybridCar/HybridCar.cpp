// HybridCar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;
/*
	The answer to the extra credit is 167 mpg
*/

/*
	Estimate miles you drive per year.
	-465
	Only positive numbers are valid. Please enter the correct number.
	465
	Estimate the price of a gallon of gas.
	2.5
	What is the cost of a hybrid car?
	25000
	What is the efficiency of the hybrid car in miles per gallon?
	34
	What is the estimated resale value for a hybrid after 5 years?
	18000
	What is the cost of a non-hybrid car?
	20000
	What is the efficiency of the non-hybrid car in miles per gallon?
	24
	What is the estimated resale value for a non-hybrid after 5 years?
	15000
	Do you want to buy based on gas consumption or total cost?
	gas

	Hybrid
	65
	7162.5

	Non-Hybrid
	95
	5237.5


	Estimate miles you drive per year.
	645
	Estimate the price of a gallon of gas.
	3.4
	What is the cost of a hybrid car?
	24000
	What is the efficiency of the hybrid car in miles per gallon?
	56
	What is the estimated resale value for a hybrid after 5 years?
	18700
	What is the cost of a non-hybrid car?
	21000
	What is the efficiency of the non-hybrid car in miles per gallon?
	27
	What is the estimated resale value for a non-hybrid after 5 years?
	16000
	Do you want to buy based on gas consumption or total cost?
	total

	Non-Hybrid
	115
	5391

	Non-Hybrid
	55
	5487


	Estimate miles you drive per year.
	964
	Estimate the price of a gallon of gas.
	3.39
	What is the cost of a hybrid car?
	27000
	What is the efficiency of the hybrid car in miles per gallon?
	37
	What is the estimated resale value for a hybrid after 5 years?
	17000
	What is the cost of a non-hybrid car?
	21300
	What is the efficiency of the non-hybrid car in miles per gallon?
	29
	What is the estimated resale value for a non-hybrid after 5 years?
	16000
	Do you want to buy based on gas consumption or total cost?
	total

	Non-Hybrid
	165
	5859.35

	Hybrid
	130
	10440.7
*/


int main()
{
	const int numberOfYearsUsed = 5;
	/* 
		Most of these variables are int because they are only estimates
		so it doesn't make sense to get into decimals
	*/
	cout << "Estimate miles you drive per year." << endl;
	int mileDrivenPerYear;
	cin >> mileDrivenPerYear;
	if (mileDrivenPerYear < 0) 
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> mileDrivenPerYear;
	}

	cout << "Estimate the price of a gallon of gas." << endl;
	double priceOfGallonOfGas;
	cin >> priceOfGallonOfGas;
	if (priceOfGallonOfGas < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> priceOfGallonOfGas;
	}

	cout << "What is the cost of a hybrid car?" << endl;
	int costOfHybridCar;
	cin >> costOfHybridCar;
	if (costOfHybridCar < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> costOfHybridCar;
	}

	cout << "What is the efficiency of the hybrid car in miles per gallon?" << endl;
	int efficiencyOfHybridCar;
	cin >> efficiencyOfHybridCar;
	if (efficiencyOfHybridCar < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> efficiencyOfHybridCar;
	}

	cout << "What is the estimated resale value for a hybrid after " << numberOfYearsUsed << " years? " << endl;
	int resaleValueOfHybridCarAfterFiveYears;
	cin >> resaleValueOfHybridCarAfterFiveYears;
	if (resaleValueOfHybridCarAfterFiveYears < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> resaleValueOfHybridCarAfterFiveYears;
	}

	cout << "What is the cost of a non-hybrid car?" << endl;
	int costOfNonHybridCar;
	cin >> costOfNonHybridCar;
	if (costOfNonHybridCar < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> costOfNonHybridCar;
	}

	cout << "What is the efficiency of the non-hybrid car in miles per gallon?" << endl;
	int efficiencyOfNonHybridCar;
	cin >> efficiencyOfNonHybridCar;
	if (efficiencyOfNonHybridCar < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> efficiencyOfNonHybridCar;
	}

	cout << "What is the estimated resale value for a non-hybrid after " << numberOfYearsUsed << " years?" << endl;
	int resaleValueOfNonHybridCarAfterFiveYears;
	cin >> resaleValueOfNonHybridCarAfterFiveYears;
	if (resaleValueOfNonHybridCarAfterFiveYears < 0)
	{
		cout << "Only positive numbers are valid. Please enter the correct number." << endl;
		cin >> resaleValueOfNonHybridCarAfterFiveYears;
	}

	cout << "Do you want to buy based on gas consumption or total cost?" << endl;
	string buyingCriteria;
	cin >> buyingCriteria;
	
	double gallonsPerYearForNonHybrid = double(mileDrivenPerYear / double(efficiencyOfNonHybridCar));
	double totalCostOfNonHybridCar = (priceOfGallonOfGas * gallonsPerYearForNonHybrid * numberOfYearsUsed) + (costOfNonHybridCar - resaleValueOfNonHybridCarAfterFiveYears);

	double gallonsPerYearForHybrid = double(mileDrivenPerYear / double(efficiencyOfHybridCar));
	double totalCostOfHybridCar = (priceOfGallonOfGas * gallonsPerYearForHybrid * numberOfYearsUsed) + (costOfHybridCar - resaleValueOfHybridCarAfterFiveYears);

	transform(buyingCriteria.begin(), buyingCriteria.end(), buyingCriteria.begin(), ::tolower);

	if (buyingCriteria.compare("gas") == 0)
	{
		if (efficiencyOfNonHybridCar > efficiencyOfHybridCar)
		{
			cout << "\nNon-Hybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForNonHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfNonHybridCar << endl;

			cout << "\nHybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfHybridCar << endl;
		}
		else {
			cout << "\nHybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfHybridCar << endl;

			cout << "\nNon-Hybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForNonHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfNonHybridCar << endl;
		}
	}
	else
	{
		if (totalCostOfNonHybridCar < totalCostOfHybridCar)
		{
			cout << "\nNon-Hybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForNonHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfNonHybridCar << endl;

			cout << "\nHybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfHybridCar << endl;
		}
		else {
			cout << "\nHybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfHybridCar << endl;

			cout << "\nNon-Hybrid" << endl;
			cout << "Total Gallons Consumed: " << gallonsPerYearForNonHybrid * numberOfYearsUsed << endl;
			cout << "Total Cost of Car" << totalCostOfNonHybridCar << endl;
		}
	}

	system("pause");
	return 0;
}
//


